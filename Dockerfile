
FROM nginx
expose 3000
# listen on port 3000 and return root page for any request
run sed -i 's/listen.*80/listen\t3000/g' /etc/nginx/conf.d/default.conf \
 && sed -i '10 a \\ttry_files /index.html =404;' /etc/nginx/conf.d/default.conf \
 && rm /docker-entrypoint.d/*
